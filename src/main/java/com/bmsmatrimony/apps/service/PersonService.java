package com.bmsmatrimony.apps.service;

import com.bmsmatrimony.apps.model.Person;

public interface PersonService {
	public void save(Person person);
}
