package com.bmsmatrimony.apps.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bmsmatrimony.apps.model.Person;
import com.bmsmatrimony.apps.repository.PersonRepository;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {
	
	@Autowired
	PersonRepository personRepository;
	
	@Override
	public void save(Person person) {
		personRepository.save(person);
		
	}

}