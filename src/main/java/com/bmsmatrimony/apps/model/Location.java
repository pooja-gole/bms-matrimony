package com.bmsmatrimony.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Location")
public class Location {
	
	@Id
    //@GeneratedValue(strategy= GenerationType.AUTO)
    //@GeneratedValue(GenerationType.AUTO)
	
	@Column(name="id")
    private long id;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="pincode")
	private String pincode;
	
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getPincode() {
	return pincode;
}
public void setPincode(String pincode) {
	this.pincode = pincode;
}

public Location(long id, String city, String state, String pincode) {
	super();
	this.id = id;
	this.city = city;
	this.state = state;
	this.pincode = pincode;
}

}
