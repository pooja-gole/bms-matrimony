package com.bmsmatrimony.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Person")
public class Person {
	
	@Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    //@GeneratedValue(GenerationType.AUTO)

	@Column(name="id")
	private long id;
	
	@Column(name="first_name")
	private String first_name;
	
	@Column(name="last_name")
	private String last_name;
	
	@Column(name="contact_no")
	private String contact_no;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="email_id")
	private String email_id;
	
	
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return first_name;
	}
	public void setFirstName(String firstName) {
		this.first_name = firstName;
	}
	public String getLastName() {
		return last_name;
	}
	public void setLastName(String lastName) {
		this.last_name = lastName;
	}
	public String getContactNo() {
		return contact_no;
	}
	public void setContactNo(String contactNo) {
		this.contact_no = contactNo;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmailId() {
		return email_id;
	}
	public void setEmailId(String emailId) {
		this.email_id = emailId;
	}
	
	public Person(int id, String firstName, String lastName, String contactNo, String gender, String emailId,
			String city) {
		super();
		this.id = id;
		this.first_name = firstName;
		this.last_name = lastName;
		this.contact_no = contactNo;
		this.gender = gender;
		this.email_id = emailId;
		
	}
}

