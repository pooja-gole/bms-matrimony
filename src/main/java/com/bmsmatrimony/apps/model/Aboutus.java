package com.bmsmatrimony.apps.model;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name="Aboutus")
public class Aboutus {
	
	@Column(name="info")
private String info;

public String getInfo() {
	return info;
}

public void setInfo(String info) {
	this.info = info;
}

public Aboutus(String info) {
	super();
	this.info = info;
}

}
