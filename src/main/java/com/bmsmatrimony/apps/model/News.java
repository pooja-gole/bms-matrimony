package com.bmsmatrimony.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="News")
public class News {
	@Id
    //@GeneratedValue(strategy= GenerationType.AUTO)
    //@GeneratedValue(GenerationType.AUTO)
	
	@Column(name="id")
	private long id;
	
	@Column(name="description")
	private String description;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public News(long id, String description) {
		super();
		this.id = id;
		this.description = description;
	}
}