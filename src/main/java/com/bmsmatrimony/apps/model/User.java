package com.bmsmatrimony.apps.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="User")
public class User {
	@Id
	//@GeneratedValue(strategy= GenerationType.AUTO)
	
	@Column(name="name")
    private String name;
	
	@Column(name="email_id")
    private String email_id;
	
	@Column(name="mobile_no")
    private  String mobile_no;
	
	
   public String getName() {
	return name;
   }
   public void setName(String name) {
	this.name = name;
   }
   public String getEmailId() {
	return email_id;
   }
   public void setEmailId(String emailId) {
	this.email_id = emailId;
   }
   public String getMobileNo() {
	return mobile_no;
   }
   public void setMobileNo(String mobileNo) {
	this.mobile_no = mobileNo;
   }
   public User(String name, String emailId, String mobileNo) {
	super();
	this.name = name;
	this.email_id = emailId;
	this.mobile_no = mobileNo;
   }
}
