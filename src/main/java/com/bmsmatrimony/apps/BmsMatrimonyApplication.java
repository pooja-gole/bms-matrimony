package com.bmsmatrimony.apps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BmsMatrimonyApplication {

	public static void main(String[] args) {
		SpringApplication.run(BmsMatrimonyApplication.class, args);
	}

}

