package com.bmsmatrimony.apps.repository;
import org.springframework.data.repository.CrudRepository;

import com.bmsmatrimony.apps.model.Person;

public interface PersonRepository extends CrudRepository<Person,Long> {
	

}
