package com.bmsmatrimony.apps.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ContactusController {
	
	@Value("${contactus.message}")
    private String message;
    
    
    @GetMapping("/contactus")
    public String main(Model model) {
        model.addAttribute("message", message);
       
     return "contactus"; //view
    }
}
   
