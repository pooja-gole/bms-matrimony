package com.bmsmatrimony.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bmsmatrimony.apps.model.Person;
import com.bmsmatrimony.apps.service.PersonService;

@Controller

public class PersonController {

	@Autowired
	PersonService personService;
	
	@RequestMapping(value="/addperson",method=RequestMethod.GET)
	public ModelAndView addPerson() {
		
		ModelAndView model= new ModelAndView();
	    
		Person person= new Person();
		model.addObject("personForm",person);
		model.setViewName("person_form");
		
		return model;
		
		
	}
	
	
}
