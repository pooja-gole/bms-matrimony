<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Person Form</title>
</head>
<body>
<div class="container">
<spring:url value="/person/savePerson" var="saveURL"/>
<h2>Person</h2>
 
   <form:form modelAttribute="personForm" method="post" action="${saveURL }" cssClass="form"/>
  <%--  <form:hidden path="id"/> --%>
   <div class="Person">
  		<label>first_name</label>
   		<form:input path="first_name" cssClass="Person" id="first_name" />
   </div>
   <div class="Person">
        <label>last_name</label>
        <form:input path="last_name" cssClass="Person" id="last_name" />
   </div>
   <button type="submit" class="btn btn-primary">Save</button>
  
</div>
</body>
</html>